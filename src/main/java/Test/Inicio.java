package Test;

import org.joda.time.LocalTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Inicio {
	
	private static final Logger log = LoggerFactory.getLogger(Inicio.class);

	public static void main(String[] args) {
		
		Saludar saludar = new Saludar();
		log.info(saludar.saludo());
		LocalTime currentTime = new LocalTime();
		log.info("La hora actual es: " + currentTime);

	}

}
